package com.cda.organisateur.Auth;

import com.cda.organisateur.Model.Role;
import com.cda.organisateur.Model.User;
import com.cda.organisateur.Repository.UserRepository;
import com.cda.organisateur.config.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    // injection du repository de la classe User
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    // méthode d'enregistrement d'un nouvel utilisateur
    public AuthentificationResponse register(RegisterRequest request) {
        var user = User.builder()
                // récupération du mail de l'utilisateur dans la requête
                .email(request.getEmail())
                // récupération et encodage du mot de passe avant de l'enregistrer dans la base de données
                .password(passwordEncoder.encode(request.getPassword()))
                // on attribue le rôle USER au nouvel utilisateur
                .role(Role.USER)
                .build();
        // on sauvegarde l'utilisateur dans la bdd
        userRepository.save(user);
        // génération du token de l'utilisateur
        var jwtToken = jwtService.generateToken(user);
        return AuthentificationResponse.builder()
                // on passe le token à la réponse que nous renvoyons au client
                .token(jwtToken)
                .build();
    }

    // méthode d'authentification d'un nouvelle utilisateur
    public AuthentificationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                // génération du token d'authentification de type username and passmord
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        // si le mail et la mot de passe sont corrects on recherche le client dans la bdd
        var user = userRepository.findUserByEmail(request.getEmail())
                .orElseThrow();
        // génération du token de l'utilisateur
        var jwtToken = jwtService.generateToken(user);
        return AuthentificationResponse.builder()
                // on passe le token à la réponse que nous renvoyons au client
                .token(jwtToken)
                .build();
    }
}
