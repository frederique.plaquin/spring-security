package com.cda.organisateur.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JwtAuthentificationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    // instanciation du service UserDetailsService de spring
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(

            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain

    )
            throws ServletException, IOException {
        Map<String, String> map = new HashMap<String, String>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        System.out.println("Requête reçue par le serveur : " + map);

            //Implémentation du authHeader auquel nous devons passer le jeton d'authentification JWT
            // chaque fois que nous faisons un call API.
            final String authHeader = request.getHeader("Authorization");

            // variable destinée à contenir le token
            final String jwt;

            // extraction du nom d'utilisateur (login) qui correspond pour nous àu mail de l'utilisateur
            final String userEmail;

            // vérification des variables si vides ou non valides on return
            if (authHeader == null || authHeader.startsWith("Bearer ")) {
                filterChain.doFilter(request, response);
                return;
            }

            // on récupère le jeton JWT dans notre authHeader
            jwt = authHeader.substring(7);
            userEmail = jwtService.extractUsername(jwt);

            // si l'utilisateur est déjà authentifié on a pas besoin de vérifier à nouveau la validité de son token
            if(userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                System.out.println(userEmail);
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
                if(jwtService.isTokenValid(jwt, userDetails)) {
                    // création du jeton d'authentification de l'utilisateur
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                            userDetails,
                            null,
                            userDetails.getAuthorities()
                    );
                    // on ajoute les details de notre requete au token
                    authToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(request)
                    );
                    // on met a jour le SecurityContextHolder avec le authToken généré ci-dessus
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
            // on passe la main au prochain filtre  pour qu'il puisse etre exécuté
            filterChain.doFilter(request, response);
    }
}
